<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\service;


use app\api\model\Order as OrderModel;
use app\api\model\Product as ProductModel;
use app\api\service\Order as OrderService;
use app\lib\enum\OrderStatusEnum;
use think\Db;
use think\Exception;
use think\Loader;
use think\Log;

// extend/WxPay/WxPay.Api.php
Loader::import('WxPay.WxPay#Api', EXTEND_PATH, '.php');

class WxNotify extends \WxPayNotify{

    public function NotifyProcess($objData, $config, &$msg)
    {
        if($objData['result_code'] == 'SUCCESS')
        {
            $orderNo = $objData['out_trade_no'];
            // 利用事务锁防止多次减库存
            Db::startTrans();
            try
            {
                $order = OrderModel::where('order_no', '=', $orderNo)->lock(true)->find();
                $orderService = new OrderService();
                $stockStatus = $orderService->checkOrderStock($order->id);
                if($stockStatus['pass'])
                {
                    $this->__updateOrderStatus($order->id, true);
                    $this->__reduceStock($stockStatus);
                }
                else
                {
                    $this->__updateOrderStatus($order->id, false);
                }

                Db::commit();
                return true;
            }
            catch (Exception $e)
            {
                Log::error($e);
                Db::rollback();
                return false;
            }
        }
        else
        {
            Db::commit();
            return true;
        }
    }

    /**
     * 减少库存
     * @param $stockStatus
     * @throws Exception
     */
    private function __reduceStock($stockStatus)
    {
        foreach ($stockStatus['pStatusArray'] as $singlePStatus)
        {
            ProductModel::where('id', '=', $singlePStatus['id'])->setDec('stock', $singlePStatus['count']);
        }
    }

    /**
     * 更新订单状态
     * @param $orderID
     * @param $success
     */
    private function __updateOrderStatus($orderID, $success)
    {
        $status = $success ? OrderStatusEnum::PAID : OrderStatusEnum::PAID_BUT_OUT_OF;
        OrderModel::where('id', '=', $orderID)->update(['status'=>$status]);
    }
}