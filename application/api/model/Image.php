<?php
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\model;

use think\Model;

class Image extends BaseModel {

    protected $hidden = ['id', 'from', 'delete_time', 'update_time'];

    /**
     * 读取器-处理表中url字段获取值
     * @param $val
     * @param $data
     * @return string
     */
    public function getUrlAttr($val, $data)
    {
        return $this->_prefixImgUrl($val, $data);
    }
}
