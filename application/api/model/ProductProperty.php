<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\model;


class ProductProperty extends BaseModel {
    protected $hidden = ['id', 'delete_time', 'product_id', 'update_time'];
}