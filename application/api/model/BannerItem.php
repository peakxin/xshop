<?php
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\model;

use think\Model;

class BannerItem extends BaseModel {

    protected $hidden = ['id', 'img_id', 'delete_time', 'banner_id', 'update_time'];

    public function img()
    {
        return $this->belongsTo('Image', 'img_id', 'id');
    }
}
