<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\model;


class User extends BaseModel {

    /**
     * 用户地址关联
     * @return \think\model\relation\HasOne
     */
    public function address()
    {
        return $this->hasOne('UserAddress', 'user_id', 'id');
    }

    /**
     * 根据openid获取用户信息
     * @param $openid
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getUserByOpenID($openid)
    {
        $user = self::where('openid',$openid)->find();
        return $user;
    }

}