<?php
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\model;

use think\Model;

class BaseModel extends Model {
    
    //php think optimize:schema [生成数据库表结构缓存]
    
    /**
     * 图片地址添加前缀
     * @param $val
     * @param $data
     * @return string
     */
    protected function _prefixImgUrl($val, $data)
    {
        if($data['from'] == 1)
        {
            return config('setting.img_prefix') . $val;
        }
        return $val;
    }
}
