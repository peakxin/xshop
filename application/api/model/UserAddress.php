<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\model;


class UserAddress extends BaseModel {

    protected $hidden = ['id', 'user_id', 'delete_time'];
}