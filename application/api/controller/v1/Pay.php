<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use app\api\service\Pay as PayService;
use app\api\service\WxNotify;
use app\api\validate\IDMustBePositiveInt;

class Pay extends BaseController {

    protected $beforeActionList = array(
        '_checkExclusiveScope' => ['only' => 'getPreOrder']
    );

    /**
     * 获取预支付订单
     * @param string $id
     * @return array|\成功时返回
     * @throws \app\lib\exception\ParameterException
     */
    public function getPreOrder($id='')
    {
        (new IDMustBePositiveInt())->goCheck();
        $pay = new PayService($id);
        return $pay->pay();
    }

    /**
     * 接收支付回调
     */
    public function receiveNotify()
    {
        $notify = new WxNotify();
        $notify->Handle();
    }
}