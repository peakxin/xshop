<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use app\api\validate\IDMustBePositiveInt;
use app\api\model\Banner as BannerModel;
use app\lib\exception\BannerMissException;

class Banner {

    /**
     * 获取指定id的Banner信息
     * @url /banner/:id
     * @http GET
     * @param $id Banner的id
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws BannerMissException
     * @throws \app\lib\exception\ParameterException
     */
    public function getBanner($id)
    {
        (new IDMustBePositiveInt())->goCheck();
        $banner = BannerModel::getBannerByID($id);
        
        if(!$banner)
        {
            throw new BannerMissException();
        }
        return $banner;
    }
}