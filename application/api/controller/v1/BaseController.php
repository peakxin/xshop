<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use app\api\service\Token as TokenService;
use app\lib\exception\ForbiddenException;
use app\lib\exception\TokenException;
use think\Controller;

class BaseController extends Controller {
    
    /**检查基础权限
     * @return bool
     * @throws ForbiddenException
     * @throws TokenException
     * @throws \think\Exception
     */
    protected function _checkPrimaryScope()
    {
        TokenService::needPrimaryScope();
    }

    /**
     * 检查User权限
     * @throws ForbiddenException
     * @throws TokenException
     */
    protected function _checkExclusiveScope()
    {
        TokenService::needExclusiveScope();
    }
}