<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\controller\v1;
use app\api\model\Category as CategoryModel;
use app\lib\exception\CategoryException;

class Category {

    public function getAllCategories()
    {
        $categories = CategoryModel::all(array(), 'img');
        if($categories->isEmpty()) throw new CategoryException();
        return $categories;
    }
}