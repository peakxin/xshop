<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\validate;

class IDMustBePositiveInt extends BaseValidate {
    
    protected $rule = array(
        'id' => 'require|isPositiveInteger',
    );
    
    protected $message = array(
        'id' => 'id必须是正整数',
    );
}