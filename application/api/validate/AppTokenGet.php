<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\validate;


class AppTokenGet extends BaseValidate {
    protected $rule = [
        'ac'=>'require|isNotEmpty',
        'se'=>'require|isNotEmpty',
    ];
}