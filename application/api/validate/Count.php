<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\validate;


class Count extends BaseValidate {
    protected $rule = [
        'count' => 'isPositiveInteger|between:1,15',
    ];
}