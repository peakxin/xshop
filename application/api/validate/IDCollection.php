<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\api\validate;


class IDCollection extends BaseValidate {
    protected $rule = array(
        'ids' => 'require|checkIDs',
    );

    protected $message = array(
        'ids' => 'ids参数必须是以逗号分隔的多个正整数',
    );
    
    protected function checkIDs($value)
    {
        $values = explode(',', $value);
        if(empty($values)) return false;

        foreach ($values as $id)
        {
            if(!$this->isPositiveInteger($id))
            {
                return false;
            }
        }

        return true;
    }
}