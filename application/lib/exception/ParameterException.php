<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;

/**
 * 通用参数类异常错误
 * Class ParameterException
 * @package app\lib\exception
 */
class ParameterException extends BaseException {
    public $code = 400;
    public $errorCode = 10000;
    public $msg = "invalid parameters";
}