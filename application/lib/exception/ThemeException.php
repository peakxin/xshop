<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;


class ThemeException extends BaseException {
    public $code = 404;
    public $errorCode = 30000;
    public $msg = "指定主题不存在，请检查主题ID";
}