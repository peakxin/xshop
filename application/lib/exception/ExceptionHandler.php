<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;

use think\Exception;
use think\exception\Handle;
use think\Log;
use think\Request;

/**
 * 重写Handle的render方法，实现自定义异常消息
 * Class ExceptionHandler
 * @package app\lib\exception
 */
class ExceptionHandler extends Handle{

    private $__code;
    private $__msg;
    private $__errorCode;
    // 需要返回客户端当前请求的URL路径

    public function render(\Exception $e)
    {
        if($e instanceof BaseException)
        {
            //如果是自定义异常，则控制http状态码，不需要记录日志
            //因为这些通常是因为客户端传递参数错误或者是用户请求造成的异常
            //不应当记录日志
            $this->__code = $e->code;
            $this->__msg = $e->msg;
            $this->__errorCode = $e->errorCode;
        }
        else
        {
            // 如果是服务器未处理的异常，将http状态码设置为500，并记录日志
            if(config('app_debug')){
                // 调试状态下需要显示TP默认的异常页面，因为TP的默认页面
                // 很容易看出问题
                return parent::render($e);
            }

            $this->__code = 500;
            $this->__msg = 'sorry，we make a mistake. (^o^)Y';
            $this->__errorCode = 999;
            $this->__recordErrorLog($e);
        }

        $request = Request::instance();
        $result = array(
            'msg' => $this->__msg,
            'errorCode' => $this->__errorCode,
            'request_url' => $request->url(),
        );
        return json($result, $this->__code);
    }

    /**
     * 将异常写入日志
     * @param Exception $e
     */
    private function __recordErrorLog(\Exception $e)
    {
        Log::record($e->getMessage(), 'error');
    }
}