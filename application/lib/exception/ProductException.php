<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;

class ProductException extends BaseException {
    public $code = 404;
    public $errorCode = 20000;
    public $msg = "指定的商品不存在，请检查参数";
}