<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;


class UserException extends BaseException {
    public $code = 404;
    public $errorCode = 60000;
    public $msg = "用户不存在";
}