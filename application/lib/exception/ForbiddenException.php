<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;


class ForbiddenException extends BaseException {
    public $code = 403;
    public $msg = '权限不够';
    public $errorCode = 10001;
}