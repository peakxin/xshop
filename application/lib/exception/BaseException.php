<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;

use think\Exception;

/**
 * 自定义异常类的基类
 * Class BaseException
 * @package app\lib\exception
 */
class BaseException extends Exception
{
    // HTTP的状态码 404,200等
    public $code = 400;
    // 错误具体信息
    public $msg = 'invalid parameters';
    // 自定义的错误码
    public $errorCode = 999;

    /**
     * 构造函数，接收一个关联数组
     * @param array $params 关联数组只应包含code、msg和errorCode，且不应该是空值
     */
    public function __construct($params=array())
    {
        if(!is_array($params)){
            return;
        }
        if(array_key_exists('code',$params)){
            $this->code = $params['code'];
        }
        if(array_key_exists('msg',$params)){
            $this->msg = $params['msg'];
        }
        if(array_key_exists('errorCode',$params)){
            $this->errorCode = $params['errorCode'];
        }
    }
}