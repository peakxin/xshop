<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;

class BannerMissException extends BaseException {
    public $code = 404;
    public $msg = '请求的Banner不存在';
    public $errorCode = 40000;
}