<?php
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------
// | 自定义配置文件
// +----------------------------------------------------------------------

// Token异常

namespace app\lib\exception;

class TokenException extends BaseException {
    public $code = 401;
    public $errorCode = 10001;
    public $msg = "Token已过期或无效Token";
}