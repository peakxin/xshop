<?php
// +----------------------------------------------------------------------
// | XShop小程序商城
// +----------------------------------------------------------------------
// | Author: Peak Xin <xinyflove@gmail.com>
// +----------------------------------------------------------------------

namespace app\lib\exception;


class SuccessMessage {
    public $code = 201; // 资源发生变化成功
    public $msg = 'ok';
    public $errorCode = 0;
}