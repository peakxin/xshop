<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

//php think optimize:route [生成路由缓存]

/*首页Banner*/
Route::get('api/:version/banner/:id', 'api/:version.Banner/getBanner');

/*首页主题*/
Route::get('api/:version/theme/', 'api/:version.Theme/getSimpleList');
/*主题详情*/
Route::get('api/:version/theme/:id', 'api/:version.Theme/getComplexOne');

Route::group('api/:version/product', function(){
    /*最新商品*/
    Route::get('recent', 'api/:version.Product/getRecent');
	/*获取某分类下的商品*/
    Route::get('by_category', 'api/:version.Product/getAllInCategory');
    /*商品详情*/
    Route::get(':id', 'api/:version.Product/geOne', [], ['id'=>'\d+']);
});

/*获取所有分类*/
Route::get('api/:version/category/all', 'api/:version.Category/getAllCategories');

/*
|--------------------------------------------------------------------------
| Token相关
|--------------------------------------------------------------------------
*/
Route::post('api/:version/token/user', 'api/:version.Token/getToken');
Route::post('api/:version/token/verify', 'api/:version.Token/verifyToken');
// 获取app的token
Route::post('api/:version/token/app', 'api/:version.Token/getAppToken');

/*
|--------------------------------------------------------------------------
| Address相关
|--------------------------------------------------------------------------
*/
Route::get('api/:version/address', 'api/:version.Address/getUserAddress');
Route::post('api/:version/address', 'api/:version.Address/createOrUpdateAddress');

/*
|--------------------------------------------------------------------------
| Order相关
|--------------------------------------------------------------------------
*/
/*下单*/
Route::post('api/:version/order', 'api/:version.Order/placeOrder');
/*获取订单详情*/
Route::get('api/:version/order/:id', 'api/:version.Order/getDetail',[], ['id'=>'\d+']);
# 发货
Route::put('api/:version/order/delivery', 'api/:version.Order/delivery');
//不想把所有查询都写在一起，所以增加by_user，很好的REST与RESTFul的区别
Route::get('api/:version/order/by_user', 'api/:version.Order/getSummaryByUser');
// 获取全部订单概要列表（分页）
Route::get('api/:version/order/paginate', 'api/:version.Order/getSummary');

/*获取预支付订单*/
Route::post('api/:version/pay/pre_order', 'api/:version.Pay/getPreOrder');
/*支付回调*/
Route::post('api/:version/pay/notify', 'api/:version.Pay/receiveNotify');