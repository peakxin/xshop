// pages/home/home.js
import { Home } from 'home-model.js';
var home = new Home();// 实例化 首页 对象

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._loadData();
  },

  /**
   * 加载所有数据
   */
  _loadData: function () {
    var id = 1;

    /*获得bannar信息*/
    home.getBannerData(id, (res)=>{
      // 数据绑定
      this.setData({
        bannerArr: res
      });
    });

    /*获取主题信息*/
    home.getThemeData((res) => {
      // 数据绑定
      this.setData({
        themeArr: res
      });
    });

    /*获取单品信息*/
    home.getProductsData((res)=>{
      // 数据绑定
      this.setData({
        productsArr: res
      });
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

  /**
   * 跳转到商品详情
   */
  ,onProductsItemTap: function (event) {
    var id = home.getDataSet(event, 'id');
    wx.navigateTo({
      url: '../product/product?id=' + id,
    });
  }

  /**
   * 跳转到主题列表
   */
  ,onThemesItemTap: function (event) {
    var id = home.getDataSet(event, 'id');
    var name = home.getDataSet(event, 'name');
    wx.navigateTo({
      url: '../theme/theme?id=' + id + '&name=' + name,
    });
  }
})