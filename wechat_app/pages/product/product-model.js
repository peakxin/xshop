/**
 * Product模型
 * @author: Peak Xin<xinyflove@sina.com>
 */
import {Base} from '../../utils/base.js';

class Product extends Base {

  constructor()
  {
    super();
  }

  /**
   * 获取获取商品详情
   */
  getDetailInfo(id, callback)
  {
    var params = {
      url: 'product/' + id,
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(params);
  }
}

export { Product };