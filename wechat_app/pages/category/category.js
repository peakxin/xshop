// pages/category/category.js
import { Category } from 'category-model.js';
var category = new Category();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentMenuIndex: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._loadData();
  },

  /**
   * 加载所有数据
   */
  _loadData: function () {
    category.getCategoryType((categoryData) => {
      this.setData({
        catagoryTypeArr: categoryData
      });
      
      // 一定在回调里再进行获取分类详情的方法调用
      category.getProductsByCategory(categoryData[0].id, (data) => {
        var dataObj = {
          procucts: data,
          topImgUrl: categoryData[0].img.url,
          title: categoryData[0].name
        };
        
        this.setData({
          categoryProducts: dataObj
        });
      });
    });

  }

  /**
   * 切换分类
   */
  , changeCategory: function (event) {
    var id = category.getDataSet(event, 'id'),
      index = category.getDataSet(event, 'index');
    category.getProductsByCategory(id, (res) => {
      this.setData({
        currentMenuIndex: index
      });
    });
  }
})