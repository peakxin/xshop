/**
 * 模型类的基类
 * @author: Peak Xin<xinyflove@sina.com>
 */
import { Config } from 'config.js';
import { Token } from 'token.js';

class Base {

  constructor()
  {
    this.baseRequestUrl = Config.restUrl;
  }

  /**
   * http 请求类
   * 当noRefatch为true时，不做未授权重试机制
   */
  request(params, noRefatch)
  {
    var that = this;
    var url = this.baseRequestUrl + params.url;

    if(!params.type)
    {
      params.type = 'GET';
    }

    wx.request({
      url: url,
      data: params.data,
      method: params.type,
      header: {
        'content-type': 'application/json',
        'token': wx.getStorageSync('token')
      },
      success: function(res)
      {
        var code = res.statusCode.toString();
        var startChar = code.charAt(0);// 状态码第一位字符
        
        if (startChar == '2')
        {
          params.sCallback && params.sCallback(res.data);
        }
        else
        {
          if (code == '401')
          {
            if (!noRefatch)
            {
              that._refetch(params);
            }
          }
          if (noRefatch)
          {
            params.eCallback && params.eCallback(res.data);
          }
        }
      },
      fail: function(err)
      {
        console.log(err);
      }
    });
  }

  /**
   * 重新获取(重发机制)
   */
  _refetch(params)
  {
    var token = new Token();
    token.getTokenFromServer((token) => {
      this.request(params, true);
    });
  }

  /**
   * 获得元素上的绑定的值
   */
  getDataSet(event, key)
  {
    return event.currentTarget.dataset[key];
  }
}

export {Base};